import argparse
import subprocess
import time
from configparser import ConfigParser
from crosspress.dict import (windows_kdict, linux_kdict, darwin_kdict, kdict)
from markright import mark

ap = argparse.ArgumentParser(description="Version iterator")
group = ap.add_mutually_exclusive_group(required=True)
group.add_argument("--feature", action="store_true", help="Feature")
group.add_argument("--minor", action="store_true", help="Minor")
group.add_argument("--major", action="store_true", help="Major")
ap.add_argument("--pub", action="store_true", help="PyPi upload")
args = ap.parse_args()

configfile_name = "iterate.ini"
config = ConfigParser()
config.read(configfile_name)

name = config["iteration"]["name"]
version_str = config["iteration"]["version"]
version_ier = list(int(el) for el in version_str.split("."))

if args.feature:
    version_ier[2] += 1

if args.minor:
    version_ier[1] += 1
    version_ier[2] = 0

if args.major:
    version_ier[0] += 1
    version_ier[1] = 0
    version_ier[2] = 0

version_new = ".".join(str(el) for el in version_ier)

print(f"{version_str} -> {version_new}")
time.sleep(1)

print("iterate.ini overwritten")
with open(configfile_name, "w") as configfile:
    config["iteration"]["version"] = version_new
    config.write(configfile)
time.sleep(1)

data = {
    "version": version_new,
    "windows": ", ".join([f"`` {i} ``" for i in sorted(windows_kdict(kdict))]),
    "linux": ", ".join([f"`` {i} ``" for i in sorted(linux_kdict(kdict))]),
    "darwin": ", ".join([f"`` {i} ``" for i in sorted(darwin_kdict(kdict))])
}

print("README.md reformatted")
mark("README.md", data)
time.sleep(1)

print("git add")
subprocess.run("git add --all", shell=True)
time.sleep(1)

print("git commit")
subprocess.run(f"git commit -m \"v{version_new}\"", shell=True)
time.sleep(1)

print("git tag")
subprocess.run(f"git tag v{version_new}", shell=True)
time.sleep(1)

print("git push")
subprocess.run("git push --tags origin main", shell=True)
time.sleep(1)

if args.pub:
    print("build")
    subprocess.run("python setup.py sdist bdist_wheel", shell=True)
    time.sleep(1)

    print("publishing")
    subprocess.run("twine upload --repository pypi dist/*", shell=True)
    time.sleep(1)

    print("clean")
    subprocess.run("rm -rf build/", shell=True)
    subprocess.run(f"rm -rf {name}.egg-info/", shell=True)
    subprocess.run("rm -rf dist/", shell=True)
    time.sleep(1)
